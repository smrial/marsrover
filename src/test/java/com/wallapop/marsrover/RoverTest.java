package com.wallapop.marsrover;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RoverTest {

    @Test
    public void moveForward_FromNorth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "n"), 10, 10);
        rover.moveForward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 1 && roverPosition.getPosY() == 5;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void moveForward_FromWest() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "w"), 10, 10);
        rover.moveForward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 0 && roverPosition.getPosY() == 4;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void moveForward_FromSouth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "s"), 10, 10);
        rover.moveForward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 1 && roverPosition.getPosY() == 3;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void moveForward_FromEast() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "e"), 10, 10);
        rover.moveForward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 2 && roverPosition.getPosY() == 4;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void moveForward_FromNorth_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "n"), 10, 5);
        rover.moveForward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 1 && roverPosition.getPosY() == 0;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void moveForward_FromWest_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(0, 4), "w"), 10, 5);
        rover.moveForward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 9 && roverPosition.getPosY() == 4;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void moveForward_FromSouth_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "s"), 10, 5);
        rover.moveForward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 1 && roverPosition.getPosY() == 4;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void moveForward_FromEast_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(9, 4), "e"), 10, 5);
        rover.moveForward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 0 && roverPosition.getPosY() == 4;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void moveBackward_FromNorth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "n"), 10, 10);
        rover.moveBackward();
        Point roverPosition = rover.getPosition();

        boolean moveBackward = false;

        if (roverPosition != null)
            moveBackward = roverPosition.getPosX() == 1 && roverPosition.getPosY() == 3;

        assertThat(moveBackward, is(Boolean.TRUE));
    }

    @Test
    public void moveBackward_FromWest() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "w"), 10, 10);
        rover.moveBackward();
        Point roverPosition = rover.getPosition();

        boolean moveBackward = false;

        if (roverPosition != null)
            moveBackward = roverPosition.getPosX() == 2 && roverPosition.getPosY() == 4;

        assertThat(moveBackward, is(Boolean.TRUE));
    }

    @Test
    public void moveBackward_FromSouth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "s"), 10, 10);
        rover.moveBackward();
        Point roverPosition = rover.getPosition();

        boolean moveBackward = false;

        if (roverPosition != null)
            moveBackward = roverPosition.getPosX() == 1 && roverPosition.getPosY() == 5;

        assertThat(moveBackward, is(Boolean.TRUE));
    }

    @Test
    public void moveBackward_FromEast() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "e"), 10, 10);
        rover.moveBackward();
        Point roverPosition = rover.getPosition();

        boolean moveBackward = false;

        if (roverPosition != null)
            moveBackward = roverPosition.getPosX() == 0 && roverPosition.getPosY() == 4;

        assertThat(moveBackward, is(Boolean.TRUE));
    }

    @Test
    public void moveBackward_FromNorth_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "n"), 10, 5);
        rover.moveBackward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 1 && roverPosition.getPosY() == 4;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void moveBackward_FromWest_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(9, 0), "w"), 10, 5);
        rover.moveBackward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 0 && roverPosition.getPosY() == 0;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void moveBackward_FromSouth_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(1, 9), "s"), 10, 5);
        rover.moveBackward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 1 && roverPosition.getPosY() == 0;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void moveBackward_FromEast_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(0, 0), "e"), 10, 5);
        rover.moveBackward();
        Point roverPosition = rover.getPosition();

        boolean moveFordward = false;

        if (roverPosition != null)
            moveFordward = roverPosition.getPosX() == 9 && roverPosition.getPosY() == 0;

        assertThat(moveFordward, is(Boolean.TRUE));
    }

    @Test
    public void turnLeft_FromNorth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "n"), 10, 5);
        rover.turnLeft();

        boolean turnLeft = rover.getDirection() == "w";

        assertThat(turnLeft, is(Boolean.TRUE));
    }

    @Test
    public void turnLeft_FromWest() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "w"), 10, 5);
        rover.turnLeft();

        boolean turnLeft = rover.getDirection() == "s";

        assertThat(turnLeft, is(Boolean.TRUE));
    }

    @Test
    public void turnLeft_FromSouth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "s"), 10, 5);
        rover.turnLeft();

        boolean turnLeft = rover.getDirection() == "e";

        assertThat(turnLeft, is(Boolean.TRUE));
    }

    @Test
    public void turnLeft_FromEast() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "e"), 10, 5);
        rover.turnLeft();

        boolean turnLeft = rover.getDirection() == "n";

        assertThat(turnLeft, is(Boolean.TRUE));
    }

    @Test
    public void turnRight_FromNorth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "n"), 10, 5);
        rover.turnRight();

        boolean turnLeft = rover.getDirection() == "e";

        assertThat(turnLeft, is(Boolean.TRUE));
    }

    @Test
    public void turnRight_FromEast() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "e"), 10, 5);
        rover.turnRight();

        boolean turnLeft = rover.getDirection() == "s";

        assertThat(turnLeft, is(Boolean.TRUE));
    }

    @Test
    public void turnRight_FromSouth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "s"), 10, 5);
        rover.turnRight();

        boolean turnLeft = rover.getDirection() == "w";

        assertThat(turnLeft, is(Boolean.TRUE));
    }

    @Test
    public void turnRight_FromWest() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "w"), 10, 5);
        rover.turnRight();

        boolean turnLeft = rover.getDirection() == "n";

        assertThat(turnLeft, is(Boolean.TRUE));
    }

    @Test
    public void getNextForwardPosition_FromNorth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "n"), 10, 10);
        Point nextPos = rover.getNextForwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 1 && nextPos.getPosY() == 5;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextForwardPosition_FromWest() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "w"), 10, 10);
        Point nextPos = rover.getNextForwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 0 && nextPos.getPosY() == 4;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextForwardPosition_FromEast() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "e"), 10, 10);
        Point nextPos = rover.getNextForwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 2 && nextPos.getPosY() == 4;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextForwardPosition_FromSouth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "s"), 10, 10);
        Point nextPos = rover.getNextForwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 1 && nextPos.getPosY() == 3;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextForwardPosition_FromNorth_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(1, 9), "n"), 10, 10);
        Point nextPos = rover.getNextForwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 1 && nextPos.getPosY() == 0;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextForwardPosition_FromWest_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(0, 4), "w"), 10, 10);
        Point nextPos = rover.getNextForwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 9 && nextPos.getPosY() == 4;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextForwardPosition_FromEast_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(9, 4), "e"), 10, 10);
        Point nextPos = rover.getNextForwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 0 && nextPos.getPosY() == 4;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextForwardPosition_FromSouth_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "s"), 10, 10);
        Point nextPos = rover.getNextForwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 1 && nextPos.getPosY() == 9;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextBackwardPosition_FromNorth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "n"), 10, 10);
        Point nextPos = rover.getNextBackwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 1 && nextPos.getPosY() == 3;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextBackwardPosition_FromWest() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "w"), 10, 10);
        Point nextPos = rover.getNextBackwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 2 && nextPos.getPosY() == 4;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextBackwardPosition_FromSouth() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "s"), 10, 10);
        Point nextPos = rover.getNextBackwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 1 && nextPos.getPosY() == 5;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextBackwardPosition_FromEast() {
        Rover rover = new Rover(new Coordinate(new Point(1, 4), "e"), 10, 10);
        Point nextPos = rover.getNextBackwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 0 && nextPos.getPosY() == 4;

        assertThat(nextFordward, is(Boolean.TRUE));
    }


    @Test
    public void getNextBackwardPosition_FromNorth_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(1, 0), "n"), 10, 10);
        Point nextPos = rover.getNextBackwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 1 && nextPos.getPosY() == 9;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextBackwardPosition_FromWest_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(9, 4), "w"), 10, 10);
        Point nextPos = rover.getNextBackwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 0 && nextPos.getPosY() == 4;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextBackwardPosition_FromSouth_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(1, 9), "s"), 10, 10);
        Point nextPos = rover.getNextBackwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 1 && nextPos.getPosY() == 0;

        assertThat(nextFordward, is(Boolean.TRUE));
    }

    @Test
    public void getNextBackwardPosition_FromEast_Wrapping() {
        Rover rover = new Rover(new Coordinate(new Point(0, 4), "e"), 10, 10);
        Point nextPos = rover.getNextBackwardPosition();

        boolean nextFordward = false;

        if (nextPos != null)
            nextFordward = nextPos.getPosX() == 9 && nextPos.getPosY() == 4;

        assertThat(nextFordward, is(Boolean.TRUE));
    }
}