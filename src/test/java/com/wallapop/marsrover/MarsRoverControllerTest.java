package com.wallapop.marsrover;

import org.junit.Test;

import java.util.List;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class MarsRoverControllerTest {

    @Test
    public void initializeMapSize_ValidMapSize() {
        MarsRoverController marsrover = new MarsRoverController();
        try {
            marsrover.initializeMapSize(1, 1);

            assertThat(true, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(false, is(Boolean.TRUE));
        }
    }

    @Test
    public void initializeMapSize_InvalidMapSize() {
        MarsRoverController marsrover = new MarsRoverController();
        try {
            marsrover.initializeMapSize(0, 0);
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Invalid map size"), is(Boolean.TRUE));
        }
    }

    @Test
    public void setObstacles_ValidPosition() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            List<Obstacle> obstacleList = new ArrayList<Obstacle>();
            obstacleList.add(new Obstacle(new Point(2, 3)));
            marsrover.setObstacles(obstacleList);

            assertThat(true, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(false, is(Boolean.TRUE));
        }
    }

    @Test
    public void setObstacles_InvalidPosition() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            List<Obstacle> obstacleList = new ArrayList<Obstacle>();
            obstacleList.add(new Obstacle(new Point(6, 3)));
            marsrover.setObstacles(obstacleList);
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Invalid position for obstacle 0"), is(Boolean.TRUE));
        }
    }

    @Test
    public void setObstacles_2ObstaclesSamePoint() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            List<Obstacle> obstacleList = new ArrayList<Obstacle>();
            obstacleList.add(new Obstacle(new Point(3, 3)));
            obstacleList.add(new Obstacle(new Point(3, 3)));
            marsrover.setObstacles(obstacleList);
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Invalid position for obstacle 1"), is(Boolean.TRUE));
        }
    }

    @Test
    public void setObstacles_UninitializedMap() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            List<Obstacle> obstacleList = new ArrayList<Obstacle>();
            obstacleList.add(new Obstacle(new Point(3, 3)));
            marsrover.setObstacles(obstacleList);
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Uninitialized map"), is(Boolean.TRUE));
        }
    }

    @Test
    public void setObstacles_NullList() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            marsrover.setObstacles(null);
            assertThat(true, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(false, is(Boolean.TRUE));
        }
    }

    @Test
    public void initializeRover_ValidRover() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            marsrover.initializeRover(new Point(2, 4), "n");
            assertThat(true, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(false, is(Boolean.TRUE));
        }
    }

    @Test
    public void initializeRover_ValidRover_WithObstacles() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            List<Obstacle> obstacleList = new ArrayList<Obstacle>();
            obstacleList.add(new Obstacle(new Point(2, 3)));
            obstacleList.add(new Obstacle(new Point(3, 3)));
            marsrover.setObstacles(obstacleList);
            marsrover.initializeRover(new Point(2, 4), "n");
            assertThat(true, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(false, is(Boolean.TRUE));
        }
    }

    @Test
    public void initializeRover_SamePositionObstacle() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            List<Obstacle> obstacleList = new ArrayList<Obstacle>();
            obstacleList.add(new Obstacle(new Point(2, 3)));
            marsrover.setObstacles(obstacleList);
            marsrover.initializeRover(new Point(2, 3), "n");
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Invalid rover position"), is(Boolean.TRUE));
        }
    }

    @Test
    public void initializeRover_UninitializedMap() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeRover(new Point(2, 3), "n");
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Uninitialized map"), is(Boolean.TRUE));
        }
    }

    @Test
    public void initializeRover_NullPosition() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            marsrover.initializeRover(null, "n");
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Invalid rover position"), is(Boolean.TRUE));
        }
    }

    @Test
    public void initializeRover_PositionOutsideMap() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            marsrover.initializeRover(new Point(6, 5), "n");
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Invalid rover position"), is(Boolean.TRUE));
        }
    }

    @Test
    public void initializeRover_InvalidDirection() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            marsrover.initializeRover(new Point(4, 4), "");
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Invalid rover position"), is(Boolean.TRUE));
        }
    }

    @Test
    public void processCommand_ValidCommand_Forward() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            marsrover.initializeRover(new Point(4, 4), "n");
            ResultCommand result = marsrover.processCommand("f");
            Coordinate roverCoordinate = result.getRoverCoordinate();
            boolean isCorrect = false;
            if (roverCoordinate != null)
                isCorrect = (roverCoordinate.equals(new Coordinate(new Point(4, 0), "n")));

            assertThat(isCorrect, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(false, is(Boolean.TRUE));
        }
    }

    @Test
    public void processCommand_ValidCommand_Backward() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            marsrover.initializeRover(new Point(4, 4), "n");
            ResultCommand result = marsrover.processCommand("b");
            Coordinate roverCoordinate = result.getRoverCoordinate();
            boolean isCorrect = false;
            if (roverCoordinate != null)
                isCorrect = (roverCoordinate.equals(new Coordinate(new Point(4, 3), "n")));

            assertThat(isCorrect, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(false, is(Boolean.TRUE));
        }
    }

    @Test
    public void processCommand_ValidCommand_Left() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            marsrover.initializeRover(new Point(4, 4), "n");
            ResultCommand result = marsrover.processCommand("l");
            Coordinate roverCoordinate = result.getRoverCoordinate();
            boolean isCorrect = false;
            if (roverCoordinate != null)
                isCorrect = (roverCoordinate.equals(new Coordinate(new Point(4, 4), "w")));

            assertThat(isCorrect, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(false, is(Boolean.TRUE));
        }
    }

    @Test
    public void processCommand_ValidCommand_Right() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            marsrover.initializeRover(new Point(4, 4), "n");
            ResultCommand result = marsrover.processCommand("r");
            Coordinate roverCoordinate = result.getRoverCoordinate();
            boolean isCorrect = false;
            if (roverCoordinate != null)
                isCorrect = (roverCoordinate.equals(new Coordinate(new Point(4, 4), "e")));

            assertThat(isCorrect, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(false, is(Boolean.TRUE));
        }
    }

    @Test
    public void processCommand_InvalidCommand() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            marsrover.initializeRover(new Point(4, 4), "n");
            ResultCommand result = marsrover.processCommand("t");
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Invalid command"), is(Boolean.TRUE));
        }
    }

    @Test
    public void processCommand_UninitializedMap() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            ResultCommand result = marsrover.processCommand("f");
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Uninitialized map"), is(Boolean.TRUE));
        }
    }

    @Test
    public void processCommand_UninitializedRover() {
        MarsRoverController marsrover = new MarsRoverController();

        try {
            marsrover.initializeMapSize(5, 5);
            ResultCommand result = marsrover.processCommand("f");
            assertThat(false, is(Boolean.TRUE));
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Uninitialized rover"), is(Boolean.TRUE));
        }
    }
}