package com.wallapop.marsrover;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MapTest {

    @Test
    public void addObstacle_ValidPoint() {
        Map map = new Map(10, 10);

        boolean isAdded = map.addObstacle(new Obstacle(1, 2));

        assertThat(isAdded, is(Boolean.TRUE));
    }

    @Test
    public void addObstacle_InvalidPoint() {
        Map map = new Map(10, 10);

        boolean isAdded = map.addObstacle(new Obstacle(11, 2));

        assertThat(isAdded, is(Boolean.FALSE));
    }

    @Test
    public void addObstacle_2ObstaclesSamePoint() {
        Map map = new Map(10, 10);

        map.addObstacle(new Obstacle(1, 2));
        boolean isAdded = map.addObstacle(new Obstacle(1, 2));

        assertThat(isAdded, is(Boolean.FALSE));
    }

    @Test
    public void addObstacle_NullPoint() {
        Map map = new Map(10, 10);

        boolean isAdded = map.addObstacle(null);

        assertThat(isAdded, is(Boolean.FALSE));
    }

    @Test
    public void pointInsideMap_ValidPoint() {
        Map map = new Map(10, 10);

        boolean isInsideMap = map.pointInsideMap(new Point(1, 1));

        assertThat(isInsideMap, is(Boolean.TRUE));
    }

    @Test
    public void pointInsideMap_InvalidPoint() {
        Map map = new Map(10, 10);

        boolean isInsideMap = map.pointInsideMap(new Point(1, 11));

        assertThat(isInsideMap, is(Boolean.FALSE));
    }

    @Test
    public void pointInsideMap_NullPoint() {
        Map map = new Map(10, 10);

        boolean isInsideMap = map.pointInsideMap(null);

        assertThat(isInsideMap, is(Boolean.FALSE));
    }

    @Test
    public void existObstacleInPoint_ExistObstacle() {
        Map map = new Map(10, 10);

        map.addObstacle(new Obstacle(1, 2));
        boolean obstacleInPoint = map.existObstacleInPoint(new Point(1, 2));

        assertThat(obstacleInPoint, is(Boolean.TRUE));
    }

    @Test
    public void existObstacleInPoint_notExistObstacle() {
        Map map = new Map(10, 10);

        map.addObstacle(new Obstacle(1, 2));
        boolean obstacleInPoint = map.existObstacleInPoint(new Point(2, 2));

        assertThat(obstacleInPoint, is(Boolean.FALSE));
    }

    @Test
    public void existObstacleInPoint_NullPoint() {
        Map map = new Map(10, 10);

        map.addObstacle(new Obstacle(1, 2));
        boolean obstacleInPoint = map.existObstacleInPoint(null);

        assertThat(obstacleInPoint, is(Boolean.FALSE));
    }

    @Test
    public void isValidDirection_ValidDirection() {
        Map map = new Map(10, 10);

        boolean isValidDirection = map.isValidDirection("n");

        assertThat(isValidDirection, is(Boolean.TRUE));
    }

    @Test
    public void isValidDirection_InvalidDirection() {
        Map map = new Map(10, 10);

        boolean isValidDirection = map.isValidDirection("t");

        assertThat(isValidDirection, is(Boolean.FALSE));
    }

    @Test
    public void isValidDirection_NullDirection() {
        Map map = new Map(10, 10);

        boolean isValidDirection = map.isValidDirection(null);

        assertThat(isValidDirection, is(Boolean.FALSE));
    }
}