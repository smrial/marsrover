package com.wallapop.marsrover;

import java.util.Arrays;

public class Rover {

    // Current horizontal, vertical and direction (facing) rover position
    private Coordinate coordinate;
    // Edge of x position
    private int maxPositionX;
    // Edge of y position
    private int maxPositionY;

    // Constructors
    public Rover(Coordinate coordinate, int maxPositionX, int maxPositionY) {
        this.coordinate = coordinate;
        this.maxPositionX = maxPositionX;
        this.maxPositionY = maxPositionY;
    }

    public Rover(Point position, String direction, int maxPositionX, int maxPositionY) {
        this.coordinate = new Coordinate(position, direction);
        this.maxPositionX = maxPositionX;
        this.maxPositionY = maxPositionY;
    }

    // Set horizontal & vertical rover position
    public void setPosition(Point position) {
        if (this.coordinate == null)
            this.coordinate = new Coordinate();

        this.coordinate.setPosition(position);
    }

    // Set rover direction (facing)
    public void setDirection(String direction) {
        if (this.coordinate == null)
            this.coordinate = new Coordinate();

        this.coordinate.setDirection(direction);
    }

    // Get the current rover position
    public Point getPosition() {
        if (this.coordinate != null)
            return this.coordinate.getPosition();
        else
            return null;
    }

    // Get current rover direction (facing)
    public String getDirection() {
        if (this.coordinate != null)
            return this.coordinate.getDirection();
        else
            return "";
    }

    // Return current rover coordinate
    public Coordinate getCoordinate() {
        return this.coordinate;
    }

    // Move the rover forward
    public void moveForward() {
        if (coordinate != null)
            coordinate.setPosition(getNextForwardPosition());
    }

    // Move the rover backward
    public void moveBackward() {
        if (coordinate != null)
            coordinate.setPosition(getNextBackwardPosition());
    }

    // Change rover direction (turn right rotation = 1, turn left rotation = -1 )
    private void rotate(int rotation){
        String[] direction = {"n", "e", "s", "w"};
        int pos = Arrays.asList(direction).indexOf(coordinate.getDirection());
        pos = pos + rotation;

        if(pos < 0)
            pos = direction.length - 1;
        else if(pos > (direction.length - 1))
            pos = 0;

        coordinate.setDirection(direction[pos]);
    }

    // Turn left rover direction
    public void turnLeft() {
        rotate(-1);
    }

    // Turn right rover direction
    public void turnRight() {
        rotate(1);
    }

    // Calculate next rover position (movement == 1 forward, movement == -1 backward)
    private Point getNextPosition(int movement) {
        Point newPosition = null;

        if (coordinate != null) {
            int nextPosX = coordinate.getPosition().getPosX();
            int nextPosY = coordinate.getPosition().getPosY();

            if (coordinate.getDirection().equals("n"))
                nextPosY += movement;
            else if (coordinate.getDirection().equals("w"))
                nextPosX -= movement;
            else if (coordinate.getDirection().equals("s"))
                nextPosY -= movement;
            else if (coordinate.getDirection().equals("e"))
                nextPosX += movement;

            newPosition = getPositionWrapping(new Point(nextPosX, nextPosY));
        }

        return newPosition;
    }

    // Calculate the next position if rover go forward
    public Point getNextForwardPosition() {
        return getNextPosition(1);
    }

    // Calculate the next position if rover go Backward
    public Point getNextBackwardPosition() {
        return getNextPosition(-1);
    }

    // Calculate real rover position in case wrapping one edge of the grid
    private Point getPositionWrapping(Point position) {
        Point realPosition = null;

        if (position != null) {
            int posX = position.getPosX();
            int posY = position.getPosY();

            if (this.maxPositionX > 0 && this.maxPositionY > 0) {
                if (posX < 0)
                    posX = this.maxPositionX - 1;
                else
                    posX = posX % this.maxPositionX;

                if (posY < 0)
                    posY = this.maxPositionY - 1;
                else
                    posY = posY % this.maxPositionY;
            }

            realPosition = new Point(posX, posY);
        }

        return realPosition;
    }
}
