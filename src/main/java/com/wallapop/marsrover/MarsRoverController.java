package com.wallapop.marsrover;

import java.util.List;

public class MarsRoverController {

    private Map map;
    private Rover rover;

    // Initialize map size
    public void initializeMapSize(int sizeX, int sizeY) throws Exception {
        if (sizeX > 0 && sizeY > 0)
            this.map = new Map(sizeX, sizeY);
        else
            throw new Exception("Invalid map size");
    }

    // Add obstacles to the map
    public void setObstacles(List<Obstacle> obstacles) throws Exception {
        if (this.map != null) {
            if (obstacles != null && obstacles.size() > 0) {
                for (int i = 0; i < obstacles.size(); i++) {
                    if (!this.map.addObstacle(obstacles.get(i)))
                        throw new Exception(String.format("Invalid position for obstacle %d", i));
                }
            }
        } else
            throw new Exception("Uninitialized map");
    }

    // Initialize rover position and direction
    public void initializeRover(Point position, String direction) throws Exception {
        if (this.map != null) {
            if (position != null && this.map.pointInsideMap(position) && !this.map.existObstacleInPoint(position) && this.map.isValidDirection(direction))
                this.rover = new Rover(position, direction, this.map.getSizeX(), this.map.getSizeY());
            else
                throw new Exception("Invalid rover position");
        } else
            throw new Exception("Uninitialized map");
    }

    public ResultCommand processCommand(String command) throws Exception {
        ResultCommand result = null;

        if (this.map != null) {
            if (this.rover != null) {
                switch (command.toLowerCase()) {
                    case "f":
                    case "b":
                        result = moveRover(command);
                        break;
                    case "l":
                    case "r":
                        result = turnRover(command);
                        break;
                    default:
                        throw new Exception("Invalid command");
                }
            } else
                throw new Exception("Uninitialized rover");
        } else
            throw new Exception("Uninitialized map");

        return result;
    }

    // Move rover forward or backward
    private ResultCommand moveRover(String command) {
        ResultCommand result = new ResultCommand();
        Point nextPos = null;

        nextPos = (command.equals("f") ? this.rover.getNextForwardPosition() : this.rover.getNextBackwardPosition());

        if (nextPos != null) {
            if (!this.map.existObstacleInPoint(nextPos)) {
                if (command.equals("f"))
                    this.rover.moveForward();
                else if (command.equals("b"))
                    this.rover.moveBackward();
            } else {
                result.setObstacleFound(true);
                result.setErrMessage(String.format("Obstacle found at x:%d y:%d", nextPos.getPosX(), nextPos.getPosY()));
            }
            result.setRoverCoordinate(this.rover.getCoordinate());
        }

        return result;
    }

    // Turn the rover to the left or right
    private ResultCommand turnRover(String command) {
        ResultCommand result = new ResultCommand();

        if (command.equals("l"))
            this.rover.turnLeft();
        else if (command.equals("r"))
            this.rover.turnRight();

        result.setRoverCoordinate(this.rover.getCoordinate());

        return result;
    }

}
