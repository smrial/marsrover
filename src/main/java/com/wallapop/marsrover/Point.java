package com.wallapop.marsrover;

import java.util.Objects;

public class Point {

    // Horizontal position
    private int posX;
    // Vertical position
    private int posY;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return posX == point.posX &&
                posY == point.posY;
    }

    @Override
    public int hashCode() {
        return Objects.hash(posX, posY);
    }

    // Constructor
    public Point(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    // Set horizontal position
    public void setPosX(int posX) {
        this.posX = posX;
    }

    // Set vertical position
    public void setPosY(int posY) {
        this.posY = posY;
    }

    // Get horizontal position
    public int getPosX() {
        return this.posX;
    }

    // Get vertical position
    public int getPosY() {
        return this.posY;
    }

}
