package com.wallapop.marsrover;

import java.util.Objects;

public class Obstacle {

    private Point position;

    // Constructor
    public Obstacle(int posX, int posY) {
        setPosition(posX, posY);
    }

    public Obstacle(Point position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Obstacle obstacle = (Obstacle) o;
        return Objects.equals(position, obstacle.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }

    // Set the position of an obstacle
    public void setPosition(int posX, int posY) {
        this.position = new Point(posX, posY);
    }

    // Get the position of an obstacle
    public Point getPosition() {
        return this.position;
    }

}
