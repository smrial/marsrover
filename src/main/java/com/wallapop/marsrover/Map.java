package com.wallapop.marsrover;

import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

public class Map {

    // Horizontal map size
    private int sizeX;
    // Vertical map size
    private int sizeY;
    // List of obstacles
    private Set<Obstacle> obstacles;

    // Constructors
    public Map() {
    }

    public Map(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }

    // Set horizontal map size
    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    // Set vertical map size
    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }

    // Get horizontal map size
    public int getSizeX() {
        return this.sizeX;
    }

    // Get vertical map size
    public int getSizeY() {
        return this.sizeY;
    }

    // Get list of obstacles
    public Set<Obstacle> getObstacles() {
        return this.obstacles;
    }

    // Add new obstacle
    public boolean addObstacle(Obstacle obstacle) {
        boolean added = false;

        if (obstacle != null && this.pointInsideMap(obstacle.getPosition())) {
            if (this.obstacles == null)
                this.obstacles = new HashSet<Obstacle>();

            if (!this.obstacles.contains(obstacle)) {
                this.obstacles.add(obstacle);
                added = true;
            }
        }

        return added;
    }

    // Check if a point is inside the map
    public boolean pointInsideMap(Point position) {
        boolean result = false;

        if (position != null)
            result = (position.getPosX() >= 0 && position.getPosX() <= (this.sizeX - 1) && position.getPosY() >= 0 && position.getPosY() <= (this.sizeY - 1));

        return result;
    }

    // Check if in point exist an obstacle
    public boolean existObstacleInPoint(Point position) {
        boolean exist = false;

        if (position != null && this.obstacles != null && this.obstacles.size() > 0) {
            Iterator<Obstacle> it = this.obstacles.iterator();

            while (it.hasNext() && !exist) {
                Obstacle o = it.next();
                exist = o.getPosition().equals(position);
            }
        }

        return exist;
    }

    // Check if direction is valid (n = north, e = east, w = west, s = south)
    public boolean isValidDirection(String direction) {
        if(direction != null){
            direction = direction.toLowerCase();
            return (direction.equals("n") || direction.equals("s") || direction.equals("e") || direction.equals("w"));
        }

        return false;
    }
}
