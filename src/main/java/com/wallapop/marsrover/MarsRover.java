package com.wallapop.marsrover;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MarsRover {

    public static void main(String[] args) {

        MarsRoverController marsrover = new MarsRoverController();

        try {
            Scanner reader = new Scanner(System.in);
            System.out.println("Insert horizontal map size:");
            int sizex = reader.nextInt();
            System.out.println("Insert vertical map size:");
            int sizey = reader.nextInt();

            // Initialize map size
            marsrover.initializeMapSize(sizex, sizey);

            System.out.println("Insert number of obstacles:");
            int obstaclesNumber = reader.nextInt();

            int x;
            int y;
            List<Obstacle> obstacles = new ArrayList<>();
            for (int i = 0; i < obstaclesNumber; i++) {
                System.out.println("Obstacle " + i);
                System.out.println("Insert coordinate x:");
                x = reader.nextInt();
                System.out.println("Insert coordinate y:");
                y = reader.nextInt();
                obstacles.add(new Obstacle(x, y));
            }

            // Set obstacles list
            marsrover.setObstacles(obstacles);

            System.out.println("Insert horizontal initial rover position:");
            int roverx = reader.nextInt();
            System.out.println("Insert vertical initial rover position:");
            int rovery = reader.nextInt();
            System.out.println("Insert initial rover direction (n = north, e = east, w = west, s = south):");
            String roverz = reader.next(); //n = north, e = east, w = west, s = south

            // Initialize rover position and direction
            marsrover.initializeRover(new Point(roverx, rovery), roverz);

            do {
                System.out.println("Insert command (f = forward, b = backward, l = turn left, r = turn right):");
                String command = reader.next();

                ResultCommand result = marsrover.processCommand(command);
                if (result != null) {
                    if (result.getObstacleFound())
                        System.out.println(result.getErrMessage());
                    else {
                        Coordinate positionRover = result.getRoverCoordinate();
                        if (positionRover != null)
                            System.out.println(String.format("Rover is at x:%d y:%d facing:%s", positionRover.getPosition().getPosX(), positionRover.getPosition().getPosY(), positionRover.getDirection()));
                    }
                }
            } while (true);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
