package com.wallapop.marsrover;

import java.util.Objects;

public class Coordinate {

    private Point position;
    private String direction;

    // Constructors
    public Coordinate() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return Objects.equals(position, that.position) &&
                Objects.equals(direction, that.direction);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, direction);
    }

    public Coordinate(Point position, String direction) {
        this.position = position;
        this.direction = direction.toLowerCase();
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public void setDirection(String direction) {
        this.direction = direction.toLowerCase();
    }

    public Point getPosition() {
        return this.position;
    }

    public String getDirection() {
        return this.direction;
    }

}
