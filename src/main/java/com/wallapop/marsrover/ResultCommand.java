package com.wallapop.marsrover;

public class ResultCommand {

    // Indicate if obstacle was found
    private boolean obstacleFound;
    // Error message
    private String errMessage;
    // Current rover coordinate
    private Coordinate roverCoordinate;

    // Constructor
    public ResultCommand() {
        this.obstacleFound = false;
        this.errMessage = "";
    }

    // Set if obstacle was found
    public void setObstacleFound(boolean obstacleFound) {
        this.obstacleFound = obstacleFound;
    }

    // Set message error
    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }

    // Set rover coordinate
    public void setRoverCoordinate(Coordinate roverCoordenate) {
        this.roverCoordinate = roverCoordenate;
    }

    // Get if obstacle was found
    public boolean getObstacleFound() {
        return obstacleFound;
    }

    // Get message error
    public String getErrMessage() {
        return errMessage;
    }

    // Get rover coordinate
    public Coordinate getRoverCoordinate() {
        return roverCoordinate;
    }

}
